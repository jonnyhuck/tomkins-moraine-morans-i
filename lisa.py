from shapely.geometry import Point
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
from pysal.lib.weights import DistanceBand as db
from pysal.lib.weights.util import min_threshold_distance
from pysal.explore.esda import Moran, Moran_Local

# global list to interpret quadrants (and style them)
quadList = ["NA", "LH", "HL", "LL", "HH"]
colors = {'NA':'#dddddd', 'LH':'cyan', 'HL':'magenta', 'LL':'blue', 'HH':'red'}

def getQuadrants(qs, sigs, acceptableSig):
    """
    * Return list of quadrant codes depending upon specified significance level
    """
    # return quad code rather than number
    out = []
    for q in range(len(qs)):
        # overrride non-significant values as N/A
        if sigs[q] < acceptableSig:
            out.append(quadList[qs[q]])
        else:
            out.append(quadList[0])
    return out


def do_lisa(gdf, col, weights):
    """
    * Do Global and Local Morans I
    """

    # calculate and report global I
    mi = Moran(gdf[col], weights, permutations=9999)
    print("\nGlobal Moran's I Results for ", m)
    print("I:\t\t", mi.I)					   # value of Moran's I
    print("z:\t\t", mi.z_sim)				   # value of Z score
    print("Simulated p:\t", mi.p_sim, "\n")   # simulated p value

    # calculate local I
    lisa = Moran_Local(gdf[col], weights, transformation='R', permutations=9999)

    # update GeoDataFrame
    newGdf = gdf.copy()
    newGdf['Morans_I'] = lisa.Is		     # The I value
    newGdf['sig'] = lisa.p_sim		     # simulated p value
    newGdf['quadrant'] = getQuadrants(lisa.q, lisa.p_sim, 0.05)  # the quadrant (1:HH 2:LH 3:LL 4:HL)

    # return the new samples
    return newGdf



# open data file as dataframe
df = pd.read_csv('Moraine_2019_06_05.csv')

# make geodataframe
gp = gpd.GeoDataFrame(df, geometry=[Point(x, y) for x, y in zip(df.Longitude, df.Latitude)])
gp.crs = {'init': 'epsg:4326'}

# get list of moraines
moraines = gp.Landform.unique()

# loop through moraines
for m in moraines:

    # get samples for current moraine
    samples = gp.loc[gp.Landform == m]

    # make raw data capable of assessment by from_dataframe
    notPoints = [[x, y] for x, y in zip(samples.Longitude, samples.Latitude)]

    # calculate weights using minimum nearest neighbour distance threshold
    weights = db.from_dataframe(samples, threshold=min_threshold_distance(notPoints), binary=False)

    # perform row standardisation (so all weights in a row add up to 1)
    weights.transform = 'r'

    # calculate lisa
    result = do_lisa(samples, 'R_Mean', weights)

    # plot to image
    f, ax = plt.subplots(1, figsize=(8, 5))

    # set colours by category map
    grouped = result.groupby('quadrant')
    # for key, group in grouped:
    #         group.plot(ax=ax, color=colors[key])

    # load into dictionary for more control
    d = {}
    for key, group in grouped:
        d[key] = group

    # plot in order of preference
    for q in quadList:
        try:
            d[q].plot(ax=ax, color=colors[q])
        except KeyError:
            pass

    # samples.plot(ax=ax, column='R_Mean', cmap='viridis_r', s=2)

    # export files
    plt.savefig("out/" + m + ".jpg")
    result.to_file("out/" + m + ".shp")

print("done")
